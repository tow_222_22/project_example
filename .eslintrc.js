module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint"
  },
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    // 不检查定义变量但未使用的情况，一定要添加这一项，不然无论是打包还是提交代码都会因为有变量定义了没使用报错
    "no-unused-vars": "off"
  },
  globals: {
    // 将高德地图的相关AMap注册到eslintrc的globals中，确保vue不会报错AMap为undefined
    AMap: false,
    AMapUI: false
  }
};
