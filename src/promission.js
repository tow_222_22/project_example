// 路由的前置守卫
// 将getUserInfo也写在这里，很厉害,省去获得token后写一次

import router from "./router";
import store from "./store";
import userApi from "@/api/user";

import { mapState, mapMutations } from "vuex";

// 刷新页面标志
let doRefresh = true;

router.beforeEach((to, from, next) => {
  if (to.path === "/login") {
    next();
  }
  // 跳转其他的操作
  const token = store.state.userTodo.token;
  if (token) {
    (async function() {
      let res;
      try {
        res = await userApi.jeesiteAuthority();
      } catch (error) {
        next("/login");
      }
      // 判断是否超时被退出
      if (
        res.data != undefined &&
        res.data.sessionid != undefined &&
        res.data.sessionid === token &&
        res.data.result
      ) {
        await doNext(to, next);
        // 点击出现tab
        store.state.commit("addTab", to);
        console.log("tab", this.$store.state.navTabs.activeTab);
      } else if (!res.result || res.result !== "login") {
        await doNext(to, next);
      } else {
        next("/login");
      }
    })();
  } else {
    next("/login");
  }
});

async function doNext(to, next) {
  // 解决刷新页面
  if (doRefresh) {
    doRefresh = false;
    store.dispatch("SetRouter", router);
    await store.dispatch("SetMenuTree");
    next({ ...to, replace: true });
  }
  next();
}
