import request from "@/utils/request";

const fn = {};

// 获取收件箱列表
fn.getInBoxList = async function() {
  let res = await request({
    url: "/email/inbox/listData",
    method: "post"
  });
  return res;
};
// 发件箱列表
fn.sendBoxList = async function() {
  let res = await request({
    url: "/email/sndbox/listData",
    method: "post"
  });
  return res.data;
};
// 打开收件箱邮件
fn.openInBox = async function(emailId) {
  let res = await request({
    url: `/email/inbox/form.json?emailId=${emailId}`,
    method: "get"
  });
  return res.data;
};
// 发送邮件
fn.sendEmail = async function(data) {
  let res = await request({
    url: "/email/save",
    method: "post",
    data: data
  });
  return res.data;
};
// 获取联系人树
fn.getTree = async function() {
  let res = await request({
    url:
      "/email/emailGroupPerson/treeData?isLoadUser=true&isAll=true&___t=:___t",
    method: "get"
  });
  return res.data;
};
// 发邮件表单初始化
fn.initEmail = async function() {
  let res = await request({
    url: "/email/writeEmailForm.json",
    method: "get"
  });
  return res.data;
};

//编辑邮件
fn.writeEmail = async function(emailId) {
  let res = await request({
    url: `/email/sndbox/form.json?emailId=${emailId}`,
    method: "get"
  });
  return res.data;
};

export default fn;
