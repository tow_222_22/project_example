import request from "@/utils/request";

export function getMenuTree() {
  return request({
    url: "/menuTree.json",
    method: "get"
  });
}
