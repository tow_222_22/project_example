import request from "@/utils/request";

// 这里存放的是关于user用户的请求
// 所以这里都用async&await来进行回调
// 注：一旦一个链条用了async&await，所有牵涉的地方都要用async

// 登录获取token
// 得到token后获取userInfo
// 用户退出

const fun = {};

fun.test = function() {
  return request({
    url: "/test",
    method: "get"
  });
};
fun.getToken = async function(username, password) {
  let res = await request({
    url: "/user/login",
    method: "post",
    data: {
      username,
      password
    }
  });
  return res.data;
};
// 获取用户信息
fun.getUser = async function(token) {
  let res = await request({
    url: `/user/info/${token}`,
    method: "get"
  });
  return res.data;
};
// ====================================--jeesite--======================
// jeesite-登录
fun.jeesiteLogin = async function(username, password) {
  let res = await request({
    url: `/login?__login=true&__ajax=json&username=${username}&password=${password}&validCode=`,
    method: "post",
    data: {
      username,
      password
    }
  });
  return res.data;
};

// jeesite-登出
fun.jeesiteLogout = async function(token) {
  let res = await request({
    url: `/logout?__ajax=json`,
    method: "post",
    data: {
      token
    }
  });
  return res.data;
};

// jeesite-获取用户权限信息
fun.jeesiteAuthority = async function(token) {
  let res = await request({
    url: "/authInfo.json",
    method: "post",
    data: {
      token
    }
  });
  return res.data;
};
// jeesite-获取用户菜单信息
fun.jeesiteUserMenu = async function(token) {
  let res = await request({
    url: "/menuTree",
    method: "post",
    data: {
      token
    }
  });
  return res.data;
};
// jeesite-重新获取登录信息
fun.jeesiteReloadLoginInfo = async function(token) {
  let res = await request({
    url: "/index.json",
    method: "post",
    data: {
      token
    }
  });
  return res.data;
};
// jeesite-获取当前用户信息
fun.jeesiteNowUserInfo = async function(token) {
  let res = await request({
    url: "/sys/user/info.json",
    method: "post"
  });
  return res.data;
};
// jeesite-获取用户权限信息
fun.jeesiteAuthority = async function(token) {
  let res = await request({
    url: "/authInfo",
    method: "get"
  });
  return res.data;
};

export default fun;
