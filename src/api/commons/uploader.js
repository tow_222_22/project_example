import request from "@/utils/request";

// 文件上传
const uploader = {
  upload(data) {
    return request({
      url: "/file/upload",
      method: "post",
      data
    });
  }
};

export default uploader;
