import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import Element from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

import "./promission"; //引入路由拦截器
import Uploader from "heart-upload";  //上传插件
import "heart-upload/lib/heart-upload.css"; //上传插件样式
import "./assets/scss/index.scss"; //引入可供全局使用的样式
import "./assets/iconfont/iconfont" //引入iconfont-symbal可用方式
import "font-awesome/css/font-awesome.min.css"; //引入font-awesome图标
import Echarts from "echarts"; //引入echarts
import Amap from "vue-amap"; //引入高德地图api
// import AmapUi from "vue-amap-ui";

Vue.config.productionTip = false;
// process.env是全局环境变量，NODE_ENV是production或者development
Vue.config.productionTip = process.env.NODE_ENV === "production";

Vue.use(Element);
Vue.use(Uploader);
Vue.use(Echarts);
Vue.use(Amap);

Vue.prototype.$Echarts = Echarts;
Vue.prototype.$Amap = Amap;

Amap.initAMapApiLoader({
  key: "d02bb0d42c40b78b5b3914ec522e804a", //高德申请码
  plugin: [
    "AMAp.Scale",
    "AMap.OverView",
    "AMap.ToolBar",
    "AMap.MapType",
    "AMap.PlaceSearch",
    "AMap.Geolocation",
    "AMap.Geocoder"
  ], //引用的插件
  v: "1.4.4", //高德SDK版本
  uiVersion: "1.0" //UI库版本
});
//引入百度统计插件
import ba from "vue-ba";
Vue.use(ba, "b0668f30d62e1597bdb36d05edea8960");

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
