import axios from "axios";
import store from "../store";
import router from "../router";
import config from "./config";

// 这里是请求的封装....比如前缀后缀的
// 还有拦截器

// 运用element-ui的消息提醒组件，来显示请求消息
import { Message } from "element-ui";
let BASE_URL;
if (process.env.NODE_ENV === "production") {
  BASE_URL = config.getConfig().serverUrl;
} else {
  BASE_URL = "/dev-api";
}

// const BASE_URL = '/dev-api';

const request = axios.create({
  baseURL: BASE_URL,
  timeout: 50000
});

// Axios提供了拦截器功能：
//  axios.interceptors.request拦截请求
//  axios.interceptors.response拦截响应

// 请求拦截器
request.interceptors.request.use(
  config => {
    // 验证token，有则进行url添加token，然后发送请求
    let token = store.state.userTodo.token;
    if (token) {
      // 这里是url只能有一个？号，如果url已有问号，则拼接为&
      let end = `__sid=${token}`;
      if (config.url.indexOf("?") > -1) {
        end = `&${end}`;
        config.url += end;
      } else {
        end = `?${end}`;
        config.url += end;
      }
    } else if (config.url.indexOf("/login") > -1) {
      let end = "__sid=";
      if (config.url.indexOf("?") > -1) {
        end = `&${end}`;
        config.url += end;
      } else {
        end = `?${end}`;
        config.url += end;
      }
    }
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);
// 响应拦截器

// 响应拦截，用于统一进行异常处理（通过状态码输出异常）
request.interceptors.response.use(
  response => {
    // 即使response.status等于200也有可能会有异常
    // 所以一般都需要自定义返回码规则
    // 统一异常处理
    // 判断会话是否过期，如果过期返回登录页
    // console.log("request.js ", response);
    if (response.data.result === "login") {
      // 清理本地的用户信息
      // 进行登出的function(){}。。。
      store.dispatch("doRemoveUser");
      router.push("/login");
    }
    // debugger;
    if (
      response.data.isValidCodeLogin != undefined &&
      !response.data.isValidCodeLogin
    ) {
      Message({
        type: "warning",
        message: response.data.message ? response.data.message : "登录失败"
      });
      router.push("/login");
    }
    const resp = response;
    // if (resp.code !== 2000 || !resp.flag) {
    if (resp.data.result != undefined && resp.data.result !== "true") {
      // console.log("res.code !== 2000");
      Message({
        type: "warning",
        message: resp.data.message ? resp.data.message : "系统异常"
      });
      return Promise.reject(response);
    }
    return response;
  },
  error => {
    // 这回后台真的抛异常了
    // 异常处理
    const resp = error.response;
    if (!resp) {
      Message({
        type: "error",
        message: "暂时连不上服务器，请与管理员联系"
      });
      return Promise.reject(error);
    }
    Message({
      type: "error",
      message:
        resp.data && resp.data.message
          ? resp.data.message
          : `系统异常，错误码为：${resp.status}`
    });
    return Promise.reject(error);
  }
);

export default request;
