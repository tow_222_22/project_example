// import { Loading } from "element-ui";

const fn = {};
const TOKEN = "user.token";
const USER_INFO = "user.info";
// 这里存放的是操作localstorage的
// 所以可以把存和取的都放在这里处理
// 存token，取token
// 存userInfo，取userInfo
// 注销token、userInfo

// 存
fn.setToken = function(_token) {
  localStorage.setItem(TOKEN, JSON.stringify(_token));
};
fn.setUserInfo = function(_userInfo) {
  localStorage.setItem(USER_INFO, JSON.stringify(_userInfo));
};

// 取
fn.getToken = function() {
  let userToken = JSON.parse(localStorage.getItem(TOKEN));
  if (userToken) {
    return userToken;
  }
};
fn.getUserInfo = function() {
  // let userInfo = JSON.parse(localStorage.getItem(USER_INFO));
  let userInfo = localStorage.getItem(USER_INFO);
  if (userInfo && userInfo != "undefined") {
    userInfo = JSON.parse(userInfo);
  }
  return userInfo;
};

// 注销（退出）
fn.logout = function() {
  localStorage.removeItem(TOKEN);
  localStorage.removeItem(USER_INFO);
};

export default fn;
