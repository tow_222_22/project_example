const CONFIG = "config";

export default {
  getConfig() {
    return JSON.parse(localStorage.getItem(CONFIG));
  }
};
