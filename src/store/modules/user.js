import userApi from "@/api/user";
import localsrgTodo from "@/utils/auth";
import Message from "element-ui";

import axios from "axios";
const user = {
  state: {
    token: localsrgTodo.getToken(),
    user: localsrgTodo.getUserInfo()
  },
  mutations: {
    SAVE_TOKEN(state, _token) {
      state.token = _token;
      localsrgTodo.setToken(_token);
    },
    SAVE_USERNAME(state, _userInfo) {
      state.user = _userInfo;
      localsrgTodo.setUserInfo(_userInfo);
    },
    REMOVE_USER(state) {
      state.token = "";
      state.user = null;
      localsrgTodo.logout();
    }
  },
  actions: {
    // async&await用try&catch方法来捕获异常
    // async setToken({ commit }, form) {
    //   try {
    //     let res = await userApi.getToken(form.account.trim(), form.psd);
    //     // console.log("user.setToken: ", res);
    //     if (res.flag) {
    //       // console.log("getToken请求成功返回的user.setToken.ok: ", res);
    //       commit("SAVE_TOKEN", res.data.token);
    //       // this.$message({type:"success",message:res.message})
    //       return res;
    //     }
    //   } catch (error) {
    //     // Message(error.message)
    //     return error;
    //   }
    // },
    // async setUserInfo({ commit, state }) {
    //   let res = await userApi.getUser(state.token);
    //   // console.log("user.setUserInfo.res", res);
    //   if (res.flag) {
    //     let info = res.data;
    //     commit("SAVE_USERNAME", info);
    //     return res;
    //   }
    // },
    // 仿jessite,得到全部信息=========================================================

    // async loginAll( { commit, state }, form ){
    // try {
    //   // let res = await userApi.jsutLogin(form.username, form.password);
    //   if( res.result ){
    //     // 得到token，存token，得到user信息，存
    //     let token = res.sessionid;
    //     let userInfo = res.user;
    //     commit("SAVE_TOKEN", token);
    //     commit("SAVE_USERNAME", userInfo);
    //     return res;
    //   }
    // } catch (error) {
    //   console.log(error)
    // }
    // let uuu = 'http://127.0.0.1:10087/unit-sbos/a/login?__login=true&__ajax=json&username=test&password=test&validCode=&__sid=';
    // axios
    //   .post(uuu)
    //   .then(response => {
    //     console.log("res ", response);
    //     debugger;
    //     let res = response.data;
    //     if(res.result){
    //       let token = res.sessionid;
    //       let userInfo = res.user;
    //       commit("SAVE_TOKEN", token);
    //       commit("SAVE_USERNAME", userInfo);
    //       return res;
    //     }
    //   })
    //   .catch(function(error) {
    //     console.log(error);
    //   });

    // },
    // ==================================realJeesite====================================================
    async loginJeesite({ commit, state }, param) {
      try {
        let res = await userApi.jeesiteLogin(param.account, param.psd);
        // console.log("store.user.js",res)
        if (res.result) {
          let token = res.sessionid;
          let userInfo = res.user;
          commit("SAVE_TOKEN", token);
          commit("SAVE_USERNAME", userInfo);
          return res;
        }
      } catch (error) {
        // console.log("user.js", error);
        return error;
      }
    },
    // ===================================================================================
    async doRemoveUser({ commit, state }) {
      // 发请求
      let res = await userApi.jeesiteLogout(state.token);
      // console.log("user.logout.res: ", res);
      if (res.result) {
        commit("REMOVE_USER");
        // Message({type:"success",message:res.message})
        return res;
      }
    }
  },
  getter: {}
};

export default user;
