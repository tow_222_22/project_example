import router from "../../router";

// import Home from "@/components/home"
const Home = resolve => require(["@/views/Home"], resolve);
const Demo = resolve => require(["@/views/Demo"], resolve);
const tabItem = {
  namespaced: true,
  state: {
    activeTabName: "/home",
    tabList: [
      {
        label: "主页",
        name: "/home",
        path: "/home",
        disabled: false,
        closable: false,
        component: Home
      },
      {
        label: "例子",
        path: "/demo",
        name: "/demo",
        disabled: false,
        closable: true,
        component: Demo
      }
    ],
    activeTab: null
  },
  getters: {
    getActiveTab(state) {
      return state.activeTab;
    }
  },
  mutations: {
    setActiveTabName(state, name) {
      state.activeTabName = name;
    },
    addTab(state, route) {
      let path, label;
      path = route.path;
      if (route.path == "/home") {
        // path = "/home";
        label = "主页";
      } else if (route.path == "/demo") {
        // path = "/demo";
        label = "例子";
      } else {
        label = route.meta.title;
      }
      // 查询过滤filter返回数组，所以第一条就是符合的记录，若有，则是tab跳转，否则添加新tab
      if (state.tabList.filter(f => f.name == path) == 0) {
        let component = resolve => require([`@/views${path}`], resolve);
        // 因为规范是组件开头大写，而path是小写，所以不匹配，这里针对demo适配
        if (path == "/demo") {
          component = Demo;
        }
        state.tabList.push({
          label: label,
          name: path,
          disabled: false,
          closable: true,
          component: component
        });
      }
      state.activeTab = route;
      state.activeTabName = path;
    },
    closeTab(state, name) {
      let nextName;
      let tab = state.tabList.filter(f => f.name == name)[0];
      let index = state.tabList.indexOf(tab);
      if (index != state.tabList.length - 1) {
        state.activeTabName = state.tabList[index + 1].name;
        nextName = state.tabList[index + 1].name;
      } else {
        state.activeTabName = state.tabList[index - 1].name;
        nextName = state.tabList[index - 1].name;
      }
      state.tabList = state.tabList.filter(f => f.name != name);
      router.push(nextName);
    },
    removeTabs(state) {
      state.activeTabName = "/home";
      state.tabList = [
        {
          label: "主页",
          name: "/home",
          path: "/home",
          disabled: false,
          closable: false,
          component: Home
        }
      ];
      state.activeTab = [];
    }
  }
};
export default tabItem;
