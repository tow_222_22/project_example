import { getMenuTree } from "@/api/menu";
import { Message } from "element-ui";

// 功能菜单数据
const menu = {
  state: {
    menuTree: null,
    router: null
  },
  getters: {
    getMenuTree(state) {
      return state.menuTree;
    }
  },
  mutations: {
    SET_MENU_TREE(state, menuTree) {
      state.menuTree = menuTree;
    },
    SET_ROUTER(state, router) {
      state.router = router;
    }
  },
  actions: {
    async SetMenuTree({ commit, state }) {
      if (state.menuTree) return;
      let response = await getMenuTree();
      if (response.data) {
        let menuTree = [];
        // 拿到默认的路由
        let router = state.router;
        let mainRoute = router.options.routes[1];
        // 根据后端返回的菜单数据动态生成路由和菜单数据
        parseMenuData(response.data, menuTree, mainRoute.children);
        // 将生成的路由覆盖原来的
        router.addRoutes(router.options.routes);
        commit("SET_MENU_TREE", menuTree);
      } else {
        Message({
          type: "warning",
          message: "获取菜单失败"
        });
      }
    },
    SetRouter({ commit }, router) {
      commit("SET_ROUTER", router);
    }
  }
};

function parseMenuData(menuData, menuTree, routes, parent) {
  try {
    doParse(menuData, menuTree, routes, parent);
  } catch (error) {
    doParse(menuData._root_, menuTree, routes, parent);
  }
}

function doParse(menuData, menuTree, routes, parent) {
  menuData.forEach(menu => {
    if (!menu.childList && !menu.menuUrl) return;
    let menuUrl = menu.menuUrl.substr(menu.menuUrl.indexOf("/a/") + 2);
    let menuItem = {
      menuCode: menu.menuCode,
      menuIcon: menu.menuIcon,
      menuName: menu.menuName,
      menuUrl: menuUrl && !menu.childList ? menuUrl : Math.random() + "",
      parent
    };
    if (menuUrl && !menu.childList) {
      let route = {
        path: menuUrl,
        component: resolve => require([`@/views${menuUrl}`], resolve),
        meta: {
          title: menu.menuName,
          menuItem
        }
      };
      routes.push(route);
    }
    if (menu.childList) {
      let children = [];
      parseMenuData(menu.childList, children, routes, menuItem);
      if (children.length > 0) {
        menuItem.children = children;
      }
    }
    console.log("menu.js--menuITem:", menuItem);
    menuTree.push(menuItem);
  });
}

export default menu;
