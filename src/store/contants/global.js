const global = {
  state: {
    BASE_URL: ""
  },
  getters: {
    getBaseUrl(state) {
      return state.BASE_URL;
    }
  },
  mutations: {
    SET_BASE_URL(state, url) {
      state.BASE_URL = url;
    }
  },
  actions: {
    SetBaseUrl({ commit }, url) {
      commit("SET_BASE_URL", url);
    }
  }
};

export default global;
