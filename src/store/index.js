import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// import auth from "@/utils/auth";
import userTodo from "@/store/modules/user";
import menu from "./modules/menu";
import global from "./contants/global";
import navTabs from "./modules/navTabs";

// 刷新后，vue实例刷新，所有东西重新挂载，store被重置，而初始值设置为null
// 所以之类需要修改store的初始值为获取localstorage的

export default new Vuex.Store({
  modules: {
    userTodo,
    menu,
    global,
    navTabs
  }
});
