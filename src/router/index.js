import Vue from "vue";
import VueRouter from "vue-router";
import store from "../store";
// import Home from "../views/Home.vue";

// 这里是路由拦截，最前置，可以写成一个js，放入main。js

import layout from "@/views/Layout";

import auth from "@/utils/auth";
import user from "@/api/user";

// import top from "@/components/layout/top";
// import left from "@/components/layout/left";
// import ct from "@/components/layout/ct";

// import home from "@/components/home";
import home from "@/views/Home";
import Login from "@/views/Login";
import demo from "@/views/Demo";

import { Message } from "element-ui";

Vue.use(VueRouter);

const routes = [
  // {
  //   path: "/",
  //   name: "Home",
  //   component: Home
  // },
  // {
  //   path: "/about",
  //   name: "About",
  //   // route level code-splitting
  //   // this generates a separate chunk (about.[hash].js) for this route
  //   // which is lazy-loaded when the route is visited.
  //   component: () =>
  //     import(/* webpackChunkName: "about" */ "../views/About.vue")
  // }
  // 登录页
  {
    path: "/login",
    component: Login
  },
  // 主桌面
  {
    path: "/",
    component: layout,
    redirect: "home",
    children: [
      {
        path: "home",
        component: home
      },
      {
        path: "/demo",
        component: demo
      }
    ]
  }
];

const router = new VueRouter({
  routes
});

export default router;
