// Vue CLI将webpack.config.js封装起来，不建议我们直接修改，用vue.config.js来代替
const path = require("path");
module.exports = {
  devServer: {
    port: 1111, //端口
    host: "localhost", //主机名
    https: false,
    open: false,
    //代理
    proxy: {
      // 匹配/dev-api的请求
      [process.env.VUE_APP_SERVICE_URI]: {
        // 用变量名来做key需要用中括号包裹
        // 代理转发地址
        target: process.env.VUE_APP_BASE_URL,
        changeOrigin: true,
        pathRewrite: {
          // 假如axios发请求到/dev-api/test
          // 经过代理的请求路径会变成http://rap2api.taobao.org/app/mock/240993/dev-api/test
          // 把路径中的/dev-api字符串替换成空字符串，就是把/dev-api去掉
          // 最终请求路径会编程http://rap2api.taobao.org/app/mock/240993/test
          ["^" + process.env.VUE_APP_SERVICE_URI]: "" // 用变量名来做key需要用中括号包裹
        }
      }
    }
  },

  lintOnSave: false,
  productionSourceMap: false,
  publicPath: "./",
  // config方式引入全局的scss
  pluginOptions: {
    "style-resources-loader": {
      preProcessor: "scss",
      patterns: [
        //加上自己的文件路径，不能使用别名
        path.resolve(__dirname, "./src/assets/scss/skin/lightBlue.scss")
      ]
    }
  }
};
